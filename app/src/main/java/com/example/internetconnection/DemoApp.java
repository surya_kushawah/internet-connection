package com.example.internetconnection;

import android.app.Application;

public class DemoApp extends Application {
  private static DemoApp mContext;

  @Override public void onCreate() {
    super.onCreate();
    mContext = this;
  }

  public static DemoApp getContext() {
    return mContext;
  }


}
