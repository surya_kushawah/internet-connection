package com.example.internetconnection.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.internetconnection.R;
import com.example.internetconnection.di.CompRoot;
import com.example.internetconnection.network.ConnectionListener;
import com.google.android.material.snackbar.Snackbar;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
  private CompRoot compRoot;
  private CompositeDisposable disposable;

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    compRoot = new CompRoot();
    disposable = new CompositeDisposable();
    addInternetConnectionListener();
  }

  CompRoot getCompRoot() {
    return compRoot;
  }

  private void addInternetConnectionListener() {
    disposable.add(ConnectionListener.getInstance()
        .listenNetworkChange().subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<Boolean>() {
          @Override public void accept(Boolean aBoolean) throws Exception {
            onInternetUnavailable();
          }
        }));
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    disposable.dispose();
  }

  protected void onInternetUnavailable() {
    showSnackBar(getString(R.string.no_internet_connection));
  }

  protected void showSnackBar(String message) {
    Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
  }

  private View getView() {
    return findViewById(android.R.id.content);
  }
}
