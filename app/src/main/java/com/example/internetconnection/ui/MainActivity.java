package com.example.internetconnection.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.example.internetconnection.R;
import com.example.internetconnection.network.ApiService;
import com.example.internetconnection.network.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

  private static final String TAG = "MainActivity";

  ApiService apiService;
  TextView textView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    // Bind Views
    textView = findViewById(R.id.textView);

    // Get ApiService service instance
    apiService = getCompRoot().getService();
    executeApiCall();
  }

  public void onRetry(View view) {
    executeApiCall();
  }

  private void executeApiCall() {
    apiService.getUsers().enqueue(new Callback<User>() {
      @Override public void onResponse(Call<User> call, Response<User> response) {
        Log.d(TAG, "onResponse: " + response.body());
        textView.setText(getString(R.string.internet_available));
      }

      @Override public void onFailure(Call<User> call, Throwable t) {
        Log.d(TAG, "onFailure: ");
      }
    });
  }

  @Override protected void onInternetUnavailable() {
    super.onInternetUnavailable();
    textView.setText(getString(R.string.no_internet_connection));
  }
}
