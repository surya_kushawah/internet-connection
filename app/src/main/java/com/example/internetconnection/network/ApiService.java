package com.example.internetconnection.network;

import com.example.internetconnection.network.model.User;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

  @GET("users")
  Call<User> getUsers();
}
