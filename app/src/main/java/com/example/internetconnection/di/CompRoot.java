package com.example.internetconnection.di;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.example.internetconnection.DemoApp;
import com.example.internetconnection.network.ApiService;
import com.example.internetconnection.network.ConnectionInterceptor;
import com.example.internetconnection.network.ConnectionListener;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompRoot {
  private Retrofit retrofit;
  private static final String BASE_URL = "https://reqres.in/api/";

  public ApiService getService() {
    if (retrofit == null) {
      retrofit = new Retrofit
          .Builder()
          .baseUrl(BASE_URL)
          .client(provideOkHttpClient())
          .addConverterFactory(GsonConverterFactory.create())
          .build();
    }

    return retrofit.create(ApiService.class);
  }

  private OkHttpClient provideOkHttpClient() {
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    httpClient.connectTimeout(30, TimeUnit.SECONDS);
    httpClient.readTimeout(30, TimeUnit.SECONDS);

    httpClient.addInterceptor(new ConnectionInterceptor() {
      @Override
      public boolean isInternetAvailable() {
        return CompRoot.this.isNetworkAvailable(DemoApp.getContext());
      }

      @Override
      public void onInternetUnavailable() {
        ConnectionListener.getInstance().notifyNetworkChange(false);
      }
    });
    return httpClient.build();
  }

  private boolean isNetworkAvailable(Context context) {
    ConnectivityManager cm =
        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
  }
}
